package test1;

public class Mainclass {

	public static void main(String[] args) {
//Deffinition des Klingonen Raumschiffs
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegha�ta");
		Ladung ladungk1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungk2 = new Ladung("Bata�leth Klingonen Schwert", 200);
		klingonen.addLadung(ladungk1);
		klingonen.addLadung(ladungk2);

//Deffinition des Romulaner Raumschiffs
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung ladungr1 = new Ladung("Borg-Schrott", 5);
		Ladung ladungr2 = new Ladung("Rote Materie", 2);
		Ladung ladungr3 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(ladungr1);
		romulaner.addLadung(ladungr2);
		romulaner.addLadung(ladungr3);
//Deffinition des Vulkanier Raumschiffs
		Raumschiff vulkanier = new Raumschiff(0, 80, 80,50, 100, 5, "Nia�Var");
		Ladung ladungv1 = new Ladung("Forschungssonde", 35);
		Ladung ladungv2 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(ladungv1);
		vulkanier.addLadung(ladungv2);
			
// Ausgabe
//		Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
		klingonen.photonentorpedoSchiessen(romulaner);
//		Die Romulaner schie�en mit der Phaserkanone zur�ck.
		romulaner.phaserkanonenSchiessen(klingonen);
//		Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");		
		
//		Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();

//		Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein (f�r Experten).
		vulkanier.reparaturDurchfuehren(false, false, false, 0);
//		Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf (f�r Experten).
		vulkanier.photonentorpedosLaden(100);
		vulkanier.ladungsverzeichnisAufraeumen();
		
//		Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
//		Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
	    klingonen.eintrageLogbuchZurueckgeben().forEach(message -> System.out.println(" Message: " + message));
	}
}
