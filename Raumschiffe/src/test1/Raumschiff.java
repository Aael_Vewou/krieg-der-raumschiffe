package test1;
import java.util.ArrayList;

public class Raumschiff
{
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
//Konstruktor ohne Parameter
	Raumschiff(){}

//Konstruktor mit Parameter
	Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebeserhaltungssystemmeInProzent, int anzahlDroiden, String schiffsname){
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebeserhaltungssystemmeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
		
	}
	public int getPhotonentorpedoAnzahl(){
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu){
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}	
	public int getEnergieversorgungInProzent(){
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int zustandEnrgieversorgungInProzentNeu){
		this.energieversorgungInProzent = zustandEnrgieversorgungInProzentNeu;
	}
	public int getSchildeInProzent(){
		return schildeInProzent;
	}
	public void setSchildeInProzent(int zustandSchildenInProzentNeu){
		this.schildeInProzent = zustandSchildenInProzentNeu;
	}
	public int getHuelleInProzent(){
		return huelleInProzent;
	}
	public void setHuelleInProzent(int zustandHuellenInProzentNeu){
		this.huelleInProzent = zustandHuellenInProzentNeu;
	}
	public int getLebenserhaltungssystemeInProzent(){
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu){
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	}
	public int getAndroidenAnzahl(){
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl){
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname(){
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname){
		this.schiffsname = schiffsname;
	}
	public void addLadung(Ladung neueLadung){
		ladungsverzeichnis.add(neueLadung);
	}
	public void photonentorpedoSchiessen(Raumschiff r){
		if (photonentorpedoAnzahl <= 0) {
			this.nachrichtAnAlle("-==*Click*==-");
		}
		else {
			frichtAnAlle("Photonentorpedo abgeschossen");
			photonentorpedoAnzahl--;
			r.treffer(r);
		}
	}
	public void phaserkanonenSchiessen(Raumschiff r){
		if(energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("-==*Click*==-");
		}
		else {
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			energieversorgungInProzent -= 50;
			r.treffer(r);
		}
	}
	private void treffer(Raumschiff r){
//		System.out.printf("%");
		
		System.out.println(" ");
		System.out.println(" || " + r.schiffsname + " wurde getroffen! ||");
		System.out.println(" ");

		int x = r.getSchildeInProzent() - 50;
		if (x <= 0) {
			r.setSchildeInProzent(0);
			int y = r.getHuelleInProzent() - 50;
			if (y <= 0) {
				r.setHuelleInProzent(0);
				System.out.println(" _______________________________________________");
				System.out.println(" Lebenserhaltungssysteme vollstaendig zerstoert!");
				System.out.println(" _______________________________________________");

			}
			else {
				r.setHuelleInProzent(y);
			}
		}
		else {
			r.setSchildeInProzent(x);
		}
		
	}
	
	public void nachrichtAnAlle(String message){
		System.out.println(" Nachricht an Alle: " + message);
		broadcastKommunikator.add(message);
	}
	public ArrayList<String> eintrageLogbuchZurueckgeben(){
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean photonentorpedosGefunden = false;
		for (Ladung element : ladungsverzeichnis){
//			Ladungsverzeichniss durchsuchen
			if(element.getBezeichnung().equals("Photonentorpedo") && element.getMenge() > 0){
				photonentorpedosGefunden = true;
				if(anzahlTorpedos > element.getMenge()) {
					this.setPhotonentorpedoAnzahl(element.getMenge());
					System.out.println(" [" + element.getMenge() + "] Photonentorpedo(s) eingesetzt");
					element.setMenge(0);
				}
				else {
					this.setPhotonentorpedoAnzahl(anzahlTorpedos);
					element.setMenge(element.getMenge()-anzahlTorpedos);
					System.out.println(" [" + anzahlTorpedos + "] Photonentorpedo(s) eingesetzt");

				}
			}
//==============================================

				
		}
		if (photonentorpedosGefunden == false) {
			System.out.println(" Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-==*Click*==-");
		}
	}
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden){
		
	}
	public void zustandRaumschiff(){
		System.out.println(" ________________________________________________________");
		System.out.printf("%-1s|", " ");
		System.out.printf("%40s \n", "Zustand der Raumschiff" + "\t\t\t" +"|");
		System.out.println(" ________________________________________________________");
		System.out.printf("%-32s:"," Schiffsname");
		System.out.printf("%24s\n", schiffsname);
		System.out.printf("%-32s:", " Photonentorpedos");
		System.out.printf("%24s\n", photonentorpedoAnzahl);
		System.out.printf("%-32s:", " Energieversorgung");
		System.out.printf("%23s", energieversorgungInProzent);
		System.out.printf("%-17s\n", "%");
		System.out.printf("%-32s:", " Schilde");
		System.out.printf("%23s", schildeInProzent);
		System.out.printf("%-17s\n", "%");
		System.out.printf("%-32s:", " Huelle");
		System.out.printf("%23s", huelleInProzent);
		System.out.printf("%-17s\n", "%");
		System.out.printf("%-32s:", " Lebenserhaltungssysteme");
		System.out.printf("%23s", lebenserhaltungssystemeInProzent);
		System.out.printf("%-17s\n", "%");
		System.out.printf("%-32s:", " Anzahl der Androiden");
		System.out.printf("%24s\n", androidenAnzahl);
		System.out.println(" ________________________________________________________");
	}
	public void ladungsverzeichnisAusgeben(){
		System.out.println(" _______________________________________________________");
		System.out.printf("%-30s:", " |Schiffsladung vom Raumschiff");
		System.out.printf("%26s\n", schiffsname + "|");
		System.out.printf("%-30s ", " |Ladungsbezeichnung");
		System.out.printf("%26s\n" + " |" , "Menge|");
		
		this.ladungsverzeichnis.forEach(element-> {
				System.out.printf("%-27s", element.getBezeichnung());
				System.out.printf("%31s", element.getMenge() + "|\n |");
		});
		
		System.out.println("______________________________________________________|");


	}
	public void ladungsverzeichnisAufraeumen(){
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0){
				ladungsverzeichnis.remove(i);
			}
		}
	}
}
