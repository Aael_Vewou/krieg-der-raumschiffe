package test1;

public class Ladung
{
	private String bezeichnung;
	private int menge;

	// Konstruktor ohne Parametern
	Ladung() {}

	// Konstruktor mit Parametern
	Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return menge;
	}
}